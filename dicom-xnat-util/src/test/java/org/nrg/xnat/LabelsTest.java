/**
 * Copyright (c) 2010 Washington University
 */
package org.nrg.xnat;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class LabelsTest extends TestCase {

	/**
	 * Test method for {@link org.nrg.xnat.Labels#isValidLabel(java.lang.CharSequence)}.
	 */
	public void testIsValidLabel() {
		assertEquals("4", Labels.toLabelChars("4"));
		assertEquals("_", Labels.toLabelChars("_"));
		assertEquals("-", Labels.toLabelChars("-"));
		assertEquals("_", Labels.toLabelChars("."));
		assertEquals("_", Labels.toLabelChars("%"));
		assertEquals("_f_o_o_", Labels.toLabelChars(",f:o^o)"));
	}

	/**
	 * Test method for {@link org.nrg.xnat.Labels#toLabelChars(java.lang.CharSequence)}.
	 */
	public void testToLabelChars() {
		assertTrue(Labels.isValidLabel("4"));
		assertTrue(Labels.isValidLabel("_"));
		assertFalse(Labels.isValidLabel("foo.bar"));
		assertFalse(Labels.isValidLabel(""));
		assertFalse(Labels.isValidLabel("*w*"));
	}

}
