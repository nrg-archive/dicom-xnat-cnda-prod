/**
 * Copyright (c) 2011 Washington University
 */
package org.nrg.dcm;

import java.util.SortedSet;

import org.dcm4che2.data.DicomObject;

/**
 * 
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public interface Extractor {
    String extract(DicomObject o);
    SortedSet<Integer> getTags();
}