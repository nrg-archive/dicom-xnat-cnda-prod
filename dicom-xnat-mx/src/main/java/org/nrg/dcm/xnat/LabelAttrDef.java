/**
 * Copyright (c) 2009,2010 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.Map;

import org.nrg.attr.ConversionFailureException;
import org.nrg.dcm.DicomAttributeIndex;
import org.nrg.xnat.Labels;

/**
 * Attribute value is changed to be a valid XNAT label.
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class LabelAttrDef extends XnatAttrDef.Abstract implements XnatAttrDef {
	private final XnatAttrDef underlying;

	public LabelAttrDef(final XnatAttrDef underlying) {
		super(underlying.getName(), underlying.getAttrs().toArray(new DicomAttributeIndex[0]));
		this.underlying = underlying;
	}

	/* (non-Javadoc)
	 * @see org.nrg.attr.ExtAttrDef#convertText(java.util.Map)
	 */
	public String convertText(final Map<DicomAttributeIndex,String> vals)
	throws ConversionFailureException {
		return Labels.toLabelChars(underlying.convertText(vals));
	}
}
