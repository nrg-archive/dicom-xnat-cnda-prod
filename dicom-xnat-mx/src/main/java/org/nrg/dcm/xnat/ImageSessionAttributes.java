/**
 * Copyright (c) 2008,2010 Washington University
 */
package org.nrg.dcm.xnat;

import org.dcm4che2.data.Tag;
import org.nrg.attr.ExtAttrDef;
import org.nrg.dcm.AttrDefs;
import org.nrg.dcm.MutableAttrDefs;
import org.nrg.dcm.DicomAttributeIndex;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
class ImageSessionAttributes {
	private ImageSessionAttributes() {}    // no instantiation
	static public AttrDefs get() { return s; }

	static final private MutableAttrDefs s = new MutableAttrDefs();
	static {
		// xnat:experimentData
		s.add(new XnatAttrDef.Date("date", Tag.StudyDate));
		s.add(new XnatAttrDef.Time("time", Tag.StudyTime));
		s.add("acquisition_site", Tag.InstitutionName);

		s.add(new ExtAttrDef.Labeled<DicomAttributeIndex,String>(new XnatAttrDef.Text("fields/field", Tag.StudyComments),
				"name", "studyComments"));

		// xnat:imageSessionData
		s.add("session_type", Tag.StudyDescription);
		s.add("modality", Tag.Modality);

		s.add(new ExtAttrDef.MultiValueWrapper<DicomAttributeIndex,String>(new XnatAttrDef.Text("scanner", Tag.StationName), ","));
		s.add("scanner/manufacturer", Tag.Manufacturer);
		s.add("scanner/model", Tag.ManufacturerModelName);

		s.add("operator", Tag.OperatorsName);
		s.add("prearchivePath");
	}
}
