/**
 * Copyright (c) 2007,2010-2011 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.Map;

import org.nrg.attr.ConversionFailureException;
import org.nrg.attr.ExtAttrValue;
import org.nrg.attr.BasicExtAttrValue;
import org.nrg.dcm.DicomAttributeIndex;

import static org.nrg.dcm.DicomAttributes.*;

import com.google.common.collect.ImmutableMap;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
final class VoxelResAttribute extends XnatAttrDef.Abstract {
    VoxelResAttribute(final String name) {
        super(name, PIXEL_SPACING, SLICE_THICKNESS);
    }

    @Override
    public ExtAttrValue convert(final Map<DicomAttributeIndex,String> attrs)
    throws ConversionFailureException {
        final String vrxy = attrs.get(PIXEL_SPACING);
        if (null == vrxy) {
            throw new ConversionFailureException(PIXEL_SPACING, null,
                    getName(), "Pixel Spacing attribute undefined");
        }
        final String[] xy = vrxy.split("\\\\");
        if (2 != xy.length) {
            throw new ConversionFailureException(PIXEL_SPACING, vrxy,
                    "Expect two values separated by delimiter \\");
        }
        final float x, y, z;
        try {
            x = Float.parseFloat(xy[0]);
            y = Float.parseFloat(xy[1]);
        } catch (NumberFormatException e) {
            throw new ConversionFailureException(PIXEL_SPACING, vrxy,
                    getName(), "cannot be translated to real numbers");
        }
        final String sliceThickness = attrs.get(SLICE_THICKNESS);
        if (null == sliceThickness) {
            throw new ConversionFailureException(SLICE_THICKNESS, null,
                    getName(), "Slice Thickness attribute undefined");
        }
        try {
            z = Float.parseFloat(attrs.get(SLICE_THICKNESS));
        } catch (NumberFormatException e) {
            throw new ConversionFailureException(SLICE_THICKNESS,
                    attrs.get(SLICE_THICKNESS), getName(),
            "cannot be translated to a real number");
        }
        return new BasicExtAttrValue(getName(), null,
                ImmutableMap.of("x", Float.toString(x), "y", Float.toString(y), "z", Float.toString(z)));
    }

    @Override
    public String convertText(final Map<DicomAttributeIndex,String> attrs) {
        return null;
    }
}
