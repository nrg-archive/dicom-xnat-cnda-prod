/**
 * Copyright 2010 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.Map;

import org.nrg.xdat.bean.ClassMapping;
import org.nrg.xdat.bean.base.BaseElement;

import com.google.common.collect.Maps;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class XnatClassMapping {
	private static final String XNAT_NS_PREFIX = "http://nrg.wustl.edu/xnat:";

	private XnatClassMapping() {}

	private static final Map<String,Class<? extends BaseElement>> beanClassCache = Maps.newHashMap();

	@SuppressWarnings("unchecked")
	public static Class<? extends BaseElement> getBeanClass(final String typeName)
	throws ClassNotFoundException {
		final String fqtn = XNAT_NS_PREFIX + typeName;
		synchronized (beanClassCache) {
			if (beanClassCache.containsKey(typeName)) {
				return beanClassCache.get(typeName);
			} else {
				final String className = ClassMapping.GetInstance().ELEMENTS.get(fqtn);
				if (null == className) {
					throw new ClassNotFoundException("no class for type " + typeName);
				}
				final Class<? extends BaseElement> clazz =
					(Class<? extends BaseElement>)
					XnatClassMapping.class.getClassLoader().loadClass(className);
				beanClassCache.put(typeName, clazz);
				return clazz;
			}
		}
	}
}
