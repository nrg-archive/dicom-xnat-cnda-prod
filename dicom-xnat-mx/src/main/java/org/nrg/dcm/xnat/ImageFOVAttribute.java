/**
 * Copyright (c) 2009-2011 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.Map;

import org.nrg.attr.BasicExtAttrValue;
import org.nrg.attr.ConversionFailureException;
import org.nrg.attr.ExtAttrValue;
import org.nrg.dcm.DicomAttributeIndex;

import static org.nrg.dcm.Attributes.*;

import com.google.common.collect.ImmutableMap;

/**
 * 
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
final class ImageFOVAttribute extends XnatAttrDef.Abstract {
    ImageFOVAttribute(final String name) {
        super(name, Rows, Cols);
    }

    @Override
    public ExtAttrValue convert(final Map<DicomAttributeIndex,String> attrs)
    throws ConversionFailureException {
        final int x, y;
        try {
            y = Integer.parseInt(attrs.get(Rows));
        } catch (NumberFormatException e) {
            throw new ConversionFailureException(Rows, attrs.get(Rows),
                    getName(), "not valid integer value");
        }
        try {
            x = Integer.parseInt(attrs.get(Cols));
        } catch (NumberFormatException e) {
            throw new ConversionFailureException(Cols, attrs.get(Cols),
                    getName(), "not valid integer value");
        }

        return new BasicExtAttrValue(getName(), null,
                ImmutableMap.of("x", Integer.toString(x), "y", Integer.toString(y)));
    }

    @Override
    public String convertText(Map<DicomAttributeIndex,String> attrs) {
        return null;
    }
}