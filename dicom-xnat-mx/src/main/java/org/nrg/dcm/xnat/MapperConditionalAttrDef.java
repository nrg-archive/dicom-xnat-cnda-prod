/**
 * Copyright (c) 2008,2010 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.Map;

import org.nrg.attr.ConversionFailureException;
import org.nrg.dcm.DicomAttributeIndex;

import com.google.common.base.Function;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class MapperConditionalAttrDef extends AbstractConditionalAttrDef {
    private final Function<String,String> mapper;

    /**
     * @param name
     * @param mapper
     * @param rules
     */
    public MapperConditionalAttrDef(final String name,
            final Function<String,String> mapper,
            final Rule... rules) {
        super(name, rules);
        this.mapper = mapper;
    }

    /*
     * (non-Javadoc)
     * @see org.nrg.attr.ExtAttrDef.Abstract#convertText(java.util.Map)
     */
    public String convertText(Map<DicomAttributeIndex,String> vals)
    throws ConversionFailureException {
        for (final Rule rule : this) {
            final String val = rule.getValue(vals);
            if (null != val) {
                final String match = mapper.apply(val);
                if (null != match) return match;
            }
        }
        return null;
    }
}
