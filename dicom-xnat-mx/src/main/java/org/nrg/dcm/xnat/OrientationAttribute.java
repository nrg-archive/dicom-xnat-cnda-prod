/**
 * Copyright (c) 2006,2007,2010 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.Map;

import org.nrg.attr.ConversionFailureException;
import org.nrg.dcm.DicomAttributeIndex;

import static org.nrg.dcm.DicomAttributes.*;

/**
 * XNAT orientation (Sag|Cor|Tra) from DICOM orientation (R^6)
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
final class OrientationAttribute extends XnatAttrDef.Abstract {
	final static String ORI_SAGITTAL = "Sag";
	final static String ORI_CORONAL = "Cor";
	final static String ORI_TRANSVERSE = "Tra";

	OrientationAttribute(final String name) {
		super(name, IMAGE_ORIENTATION_PATIENT);
	}

	@Override
	public String convertText(final Map<DicomAttributeIndex,String> attrs)
	throws ConversionFailureException {
		String dcmo = attrs.get(IMAGE_ORIENTATION_PATIENT);
		if (dcmo == null) {
			throw new ConversionFailureException(IMAGE_ORIENTATION_PATIENT, dcmo,
					"DICOM Image Orientation (Patient) field is missing or blank");
		}
		final String[] components = dcmo.split("\\\\");

		// Orientation : row cosine vector (x,y,z), then column
		// cosine vector (x,y,z)
		if (components.length != 6) {
			throw new ConversionFailureException(IMAGE_ORIENTATION_PATIENT, dcmo, getName(),
					"cannot be translated to cosine vectors");
		}

		final double[] o = new double[6];
		try {
			for (int i = 0; i < 6; i++) {
				o[i] = Double.parseDouble(components[i]);
			}
		} catch (NumberFormatException e) {
			throw new ConversionFailureException(IMAGE_ORIENTATION_PATIENT, dcmo, getName(),
					"cannot be translated to cosine vectors");
		}

		// consistency checks
		final double epsilon = 0.001;
		if (Math.abs(o[0] * o[3] + o[1] * o[4] + o[2] * o[5]) > 0.001) {
			throw new ConversionFailureException(IMAGE_ORIENTATION_PATIENT, dcmo, getName(),
					"cosine vectors not orthogonal");
		}

		if ((Math.abs(1.0 - o[0] * o[0] - o[1] * o[1] - o[2] * o[2]) > epsilon)
				|| (Math.abs(1.0 - o[3] * o[3] - o[4] * o[4] - o[5] * o[5]) > epsilon)) {
			throw new ConversionFailureException(IMAGE_ORIENTATION_PATIENT, dcmo, getName(),
					"cosine vectors not normal");
		}

		// cross product gives direction of normal to image plane
		final double absNormalX = Math.abs(o[1] * o[5] - o[2] * o[4]);
		final double absNormalY = Math.abs(o[2] * o[3] - o[0] * o[5]);
		final double absNormalZ = Math.abs(o[0] * o[4] - o[1] * o[3]);

		// patient's body length is Z, ventral-dorsal is Y, and right-left is Z,
		// so image plane normal along X->sagittal, Y->coronal, Z->transverse
		if (absNormalX > absNormalY) {
			return (absNormalX > absNormalZ) ? ORI_SAGITTAL : ORI_TRANSVERSE;
		} else {
			return (absNormalY > absNormalZ) ? ORI_CORONAL : ORI_TRANSVERSE;
		}
	}
}
