/**
 * Copyright (c) 2009-2010 Washington University
 */
package org.nrg.dcm.xnat;

import org.dcm4che2.data.Tag;
import org.nrg.dcm.AttrDefs;
import org.nrg.dcm.MutableAttrDefs;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
final class RTSessionAttributes {
	private RTSessionAttributes() {}    // no instantiation
	static public AttrDefs get() { return s; }

	static final private MutableAttrDefs s = new MutableAttrDefs(ImageSessionAttributes.get());
	static {
		s.add("UID", Tag.StudyInstanceUID);  
		s.add("dcmAccessionNumber", Tag.AccessionNumber);
		s.add("dcmPatientId", Tag.PatientID);
		s.add("dcmPatientName", Tag.PatientName);
	}
}
