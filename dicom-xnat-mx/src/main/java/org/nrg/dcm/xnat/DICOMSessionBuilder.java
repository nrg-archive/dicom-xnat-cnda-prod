/**
 * Copyright (c) 2008-2011 Washington University
 */
package org.nrg.dcm.xnat;

import java.io.Closeable;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.regex.Pattern;

import org.dcm4che2.data.UID;
import org.nrg.attr.ConversionFailureException;
import org.nrg.attr.ExtAttrDef;
import org.nrg.attr.ExtAttrValue;
import org.nrg.attr.NoUniqueValueException;
import org.nrg.attr.Utils;
import org.nrg.dcm.AttrAdapter;
import org.nrg.dcm.AttrDefs;
import org.nrg.dcm.MutableAttrDefs;
import org.nrg.dcm.DicomAttributeIndex;
import org.nrg.dcm.DicomMetadataStore;
import org.nrg.dcm.EnumeratedMetadataStore;
import org.nrg.dcm.SOPModel;
import org.nrg.dcm.xnat.AbstractConditionalAttrDef.ContainsAssignmentRule;
import org.nrg.dcm.xnat.AbstractConditionalAttrDef.EqualsRule;
import org.nrg.dcm.xnat.AbstractConditionalAttrDef.Rule;
import org.nrg.io.RelativePathWriterFactory;
import org.nrg.io.ScanCatalogFileWriterFactory;
import org.nrg.session.BeanBuilder;
import org.nrg.ulog.FileMicroLogFactory;
import org.nrg.ulog.MicroLogFactory;
import org.nrg.ulog.MicroLog;
import org.nrg.xdat.bean.XnatCrsessiondataBean;
import org.nrg.xdat.bean.XnatCtsessiondataBean;
import org.nrg.xdat.bean.XnatExperimentdataFieldBean;
import org.nrg.xdat.bean.XnatImagesessiondataBean;
import org.nrg.xdat.bean.XnatMrsessiondataBean;
import org.nrg.xdat.bean.XnatOptsessiondataBean;
import org.nrg.xdat.bean.XnatPetsessiondataBean;
import org.nrg.xdat.bean.XnatRtsessiondataBean;
import org.nrg.xdat.bean.XnatUssessiondataBean;
import org.nrg.xdat.bean.XnatXasessiondataBean;

import static org.nrg.dcm.Attributes.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * Generates an XNAT session metadata XML from a DICOM study.
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
public class DICOMSessionBuilder extends org.nrg.session.SessionBuilder implements Closeable {
    private final static String XML_SUFFIX = ".xml";

    private final Logger logger = LoggerFactory.getLogger(DICOMSessionBuilder.class);

    private final static SortedSet<DicomAttributeIndex> ALL_TAGS;

    private final DicomMetadataStore store;
    private final File fsdir;
    private final String studyInstanceUID;

    private Integer nScans = null;
    private final MutableAttrDefs sessionAttrDefs = new MutableAttrDefs();
    private final MutableAttrDefs scanAttrDefs = new MutableAttrDefs();
    private final RelativePathWriterFactory catalogWriterFactory;
    private final MicroLogFactory logWriterFactory;

    // Helpers for building subbeans
    private final static Map<String,BeanBuilder> imageSessionBeanBuilders = Maps.newHashMap();
    static {
        imageSessionBeanBuilders.put("fields/field", new BeanBuilder() {
            public Collection<XnatExperimentdataFieldBean> buildBeans(final ExtAttrValue value) {
                final XnatExperimentdataFieldBean field = new XnatExperimentdataFieldBean();
                final String text = value.getText();
                if (Strings.isNullOrEmpty(text) || "null".equals(text)) {
                    return Collections.emptyList();
                } else {
                    field.setField(text);
                    field.setName(value.getAttrs().get("name"));
                    return Collections.singletonList(field);
                }
            }
        });
    }

    private final static Map<String,Class<? extends XnatImagesessiondataBean>> modalitySessionBeanClasses = Maps.newHashMap();
    private final static Map<String,Class<? extends XnatImagesessiondataBean>> sopSessionBeanClasses = Maps.newHashMap();
    private final static Map<Class<? extends XnatImagesessiondataBean>,AttrDefs> sessionTypeAttrs = Maps.newHashMap();
    private final static Map<Class<? extends XnatImagesessiondataBean>,Map<String,BeanBuilder>> sessionBeanBuilders = Maps.newHashMap();

    private final static String UNKNOWN_MODALITY = "OT";  // DICOM: Other

    static {
        sopSessionBeanClasses.put(UID.MRImageStorage, XnatMrsessiondataBean.class);
        sopSessionBeanClasses.put(UID.EnhancedMRImageStorage, XnatMrsessiondataBean.class);
        modalitySessionBeanClasses.put("MR", XnatMrsessiondataBean.class);
        sessionTypeAttrs.put(XnatMrsessiondataBean.class, MRSessionAttributes.get());
        sessionBeanBuilders.put(XnatMrsessiondataBean.class, Maps.newHashMap(imageSessionBeanBuilders));

        sopSessionBeanClasses.put(UID.PositronEmissionTomographyImageStorage, XnatPetsessiondataBean.class);
        sopSessionBeanClasses.put(UID.EnhancedPETImageStorage, XnatPetsessiondataBean.class);
        modalitySessionBeanClasses.put("PT", XnatPetsessiondataBean.class);
        sessionTypeAttrs.put(XnatPetsessiondataBean.class, PETSessionAttributes.get());
        sessionBeanBuilders.put(XnatPetsessiondataBean.class, Maps.newHashMap(imageSessionBeanBuilders));

        sopSessionBeanClasses.put(UID.CTImageStorage, XnatCtsessiondataBean.class);
        sopSessionBeanClasses.put(UID.EnhancedCTImageStorage, XnatCtsessiondataBean.class);
        modalitySessionBeanClasses.put("CT", XnatCtsessiondataBean.class);
        sessionTypeAttrs.put(XnatCtsessiondataBean.class, CTSessionAttributes.get());
        sessionBeanBuilders.put(XnatCtsessiondataBean.class, Maps.newHashMap(imageSessionBeanBuilders));

        sopSessionBeanClasses.put(UID.XRayAngiographicImageStorage, XnatXasessiondataBean.class);
        modalitySessionBeanClasses.put("XA", XnatXasessiondataBean.class);
        sessionTypeAttrs.put(XnatXasessiondataBean.class, XASessionAttributes.get());
        sessionBeanBuilders.put(XnatXasessiondataBean.class, Maps.newHashMap(imageSessionBeanBuilders));

        sopSessionBeanClasses.put(UID.UltrasoundImageStorage, XnatUssessiondataBean.class);
        sopSessionBeanClasses.put(UID.UltrasoundMultiframeImageStorage, XnatUssessiondataBean.class);
        modalitySessionBeanClasses.put("US", XnatUssessiondataBean.class);
        sessionTypeAttrs.put(XnatUssessiondataBean.class, USSessionAttributes.get());
        sessionBeanBuilders.put(XnatUssessiondataBean.class, Maps.newHashMap(imageSessionBeanBuilders));

        sopSessionBeanClasses.put(UID.RTImageStorage, XnatRtsessiondataBean.class);
        sopSessionBeanClasses.put(UID.RTDoseStorage, XnatRtsessiondataBean.class);
        modalitySessionBeanClasses.put("RT", XnatRtsessiondataBean.class);
        sessionTypeAttrs.put(XnatRtsessiondataBean.class, RTSessionAttributes.get());
        sessionBeanBuilders.put(XnatRtsessiondataBean.class, Maps.newHashMap(imageSessionBeanBuilders));

        sopSessionBeanClasses.put(UID.ComputedRadiographyImageStorage, XnatCrsessiondataBean.class);
        modalitySessionBeanClasses.put("CR", XnatCrsessiondataBean.class);
        sessionTypeAttrs.put(XnatCrsessiondataBean.class, CRSessionAttributes.get());
        sessionBeanBuilders.put(XnatCrsessiondataBean.class, Maps.newHashMap(imageSessionBeanBuilders));

        sopSessionBeanClasses.put(UID.OphthalmicTomographyImageStorage, XnatOptsessiondataBean.class);
        modalitySessionBeanClasses.put("OPT", XnatOptsessiondataBean.class);
        sessionTypeAttrs.put(XnatOptsessiondataBean.class, OPTSessionAttributes.get());
        sessionBeanBuilders.put(XnatOptsessiondataBean.class, Maps.newHashMap(imageSessionBeanBuilders));

        final SortedSet<DicomAttributeIndex> tags = newAttributeIndexSortedSet();
        tags.add(SOPClassUID);
        tags.add(SeriesNumber);
        tags.add(StudyInstanceUID);
        tags.add(SeriesInstanceUID);
        tags.add(Modality);
        tags.addAll(CatalogAttributes.get().getNativeAttrs());
        tags.addAll(ImageFileAttributes.get().getNativeAttrs());
        for (final AttrDefs attrDefs : sessionTypeAttrs.values()) {
            tags.addAll(attrDefs.getNativeAttrs());
        }
        tags.addAll(DICOMScanBuilder.getNativeTypeAttrs());
        ALL_TAGS = Collections.unmodifiableSortedSet(tags);
    }


    private static <K,C> C getInstance(final K key,
            final Map<String,Class<? extends C>> classes) {
        final Class<? extends C> clazz = classes.get(key);
        if (null == clazz) {
            return null;
        } else {
            try {
                return clazz.newInstance();
            } catch (InstantiationException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            }
        }
    }


    private static final Comparator<DicomAttributeIndex> COMPARATOR = new DicomAttributeIndex.Comparator();
    private static SortedSet<DicomAttributeIndex> newAttributeIndexSortedSet() {
        return Sets.newTreeSet(null == COMPARATOR ? new DicomAttributeIndex.Comparator() : COMPARATOR);
    }

    /**
     * Creates a session builder that sends output to the given Writer.
     * @param fsdir Root directory of the DICOM fileset
     * @param writer Where the XML session document gets written;
     *   if null, the document is written to a file with the same name as the root directory,
     *   plus the .xml suffix
     * @param sessionAttrDefs additional definitions for session-level attributes
     * @param scanAttrDefs additional definitions for scan-level attributes
     * @throws IOException
     */
    public DICOMSessionBuilder(final DicomMetadataStore store, final String studyInstanceUID,
            final File fsdir, final Writer writer,
            final Collection<XnatAttrDef> sessionAttrDefs,
            final Collection<XnatAttrDef> scanAttrDefs) throws IOException {
        super(fsdir, fsdir.getPath(), writer, new File(fsdir.getPath() + XML_SUFFIX));
        this.store = store;
        this.studyInstanceUID = studyInstanceUID;

        this.fsdir = fsdir.getCanonicalFile();
        this.sessionAttrDefs.addAll(sessionAttrDefs);
        this.scanAttrDefs.addAll(scanAttrDefs);
        this.catalogWriterFactory = new ScanCatalogFileWriterFactory(fsdir);
        this.logWriterFactory = new FileMicroLogFactory(fsdir);
    }

    public DICOMSessionBuilder(final DicomMetadataStore store,
            final File fsdir, final Writer writer,
            final Collection<XnatAttrDef> sessionAttrDefs,
            final Collection<XnatAttrDef> scanAttrDefs)
    throws IOException,NoUniqueSessionException,SQLException {
        this(store, getStudyInstanceUID(store), fsdir, writer, sessionAttrDefs, scanAttrDefs);
    }

    @SuppressWarnings("unchecked")
    public DICOMSessionBuilder(final File fsdir, final Writer writer,
            final Collection<XnatAttrDef> sessionAttrDefs,
            final Collection<XnatAttrDef> scanAttrDefs)
    throws IOException,NoUniqueSessionException,SQLException {
        this(getStore(fsdir, getTags(sessionAttrDefs, scanAttrDefs)),
                fsdir, writer,
                sessionAttrDefs, scanAttrDefs);
    }

    /**
     * Creates a session builder that sends output to the given Writer.
     * @param fsdir Root directory of the DICOM fileset
     * @param writer Where the XML session document gets written
     * @param sessionAttrs additional definitions for session-level attributes
     * @throws IOException
     */
    public DICOMSessionBuilder(final File fsdir, final Writer writer, final XnatAttrDef...sessionAttrs)
    throws IOException,NoUniqueSessionException,SQLException {
        this(fsdir, writer, Arrays.asList(sessionAttrs), new ArrayList<XnatAttrDef>());
    }

    /**
     * Creates a session builder that writes output to a file with the
     * same name as the file set root, plus a '.xml' suffix.
     * @param fsdir Root directory of the DICOM fileset
     * @param sessionAttrs additiona definitions for session-level attributes
     * @throws IOException
     */
    public DICOMSessionBuilder(final File fsdir, final XnatAttrDef...sessionAttrs)
    throws IOException,NoUniqueSessionException,SQLException {
        this(fsdir, null, sessionAttrs);
    }

    private static String getStudyInstanceUID(final DicomMetadataStore store)
    throws IOException,NoUniqueSessionException,SQLException {
        try {
            final Set<String> uids = store.getUniqueValues(StudyInstanceUID);
            if (uids.isEmpty()) {
                throw new NoUniqueSessionException(new String[0]);
            }
            final Iterator<String> i = uids.iterator();
            final String uid = i.next();
            if (i.hasNext()) {
                throw new NoUniqueSessionException(uids.toArray(new String[0]));
            }
            return uid;
        } catch (ConversionFailureException e) {
            throw new RuntimeException(e);    // UIDs should require no conversion
        }
    }

    private static DicomMetadataStore getStore(final File root, final Collection<DicomAttributeIndex> extraTags)
    throws IOException,SQLException {
        final SortedSet<DicomAttributeIndex> tags = newAttributeIndexSortedSet();
        tags.addAll(ALL_TAGS);
        tags.addAll(extraTags);
        final DicomMetadataStore store = EnumeratedMetadataStore.createHSQLDBBacked(tags);
        store.add(Collections.singleton(root));
        return store;
    }

    private static SortedSet<DicomAttributeIndex> getTags(final Collection<XnatAttrDef>...attrDefs) {
        final SortedSet<DicomAttributeIndex> tags = newAttributeIndexSortedSet();
        for (final Collection<XnatAttrDef> defs : attrDefs) {
            for (final XnatAttrDef def : defs) {
                tags.addAll(def.getAttrs());
            }
        }
        return tags;
    }


    public static SortedSet<DicomAttributeIndex> getAttributeTags() {
        return ALL_TAGS;
    }


    /*
     * (non-Javadoc)
     * @see java.util.concurrent.Callable#call()
     */
    @SuppressWarnings("unchecked")
    public XnatImagesessiondataBean call()
    throws IOException,NoUniqueSessionException,SQLException {
        logger.info("Building session for {}", studyInstanceUID);
        final MicroLog sessionLog = logWriterFactory.getLog("dcmtoxnat.log");
        try {
            final XnatImagesessiondataBean session;
            try {
                final String leadSOPClass = SOPModel.getLeadSOPClass(store.getUniqueValues(SOPClassUID));
                final XnatImagesessiondataBean sopSession = getInstance(leadSOPClass, sopSessionBeanClasses);
                if (null == sopSession) {
                    // SOP Class doesn't determine a session type.  Let's try the lead modality.
                    final String modality = SOPModel.getLeadModality(store.getUniqueValues(Modality));
                    session = getInstance(modality, modalitySessionBeanClasses);

                    if (null == session) {
                        final String message = "Session builder not implemented for SOP class "
                            + store.getUniqueValues(SOPClassUID)
                            + " or modality " + store.getUniqueValues(Modality);
                        logger.error(message);
                        sessionLog.log(message);
                        return null;
                    }
                } else {
                    session = sopSession;
                }
            } catch (ConversionFailureException e) {
                throw new RuntimeException("no conversion necessary for SOP Class UID", e);
            }

            final AttrAdapter sessionAttrs = new AttrAdapter(store,
                    Collections.singletonMap(StudyInstanceUID, studyInstanceUID));
            sessionAttrs.add(sessionAttrDefs);
            sessionAttrs.add(sessionTypeAttrs.get(session.getClass()));

            final Map<ExtAttrDef<DicomAttributeIndex,String>,Exception> failures = Maps.newHashMap();
            final List<ExtAttrValue> sessionValues = getValues(sessionAttrs, failures);
            for (final Map.Entry<ExtAttrDef<DicomAttributeIndex,String>,Exception> me: failures.entrySet()) {
                if ("UID".equals(me.getKey().getName())) {
                    final Exception cause = me.getValue();
                    if (cause instanceof NoUniqueValueException) {
                        throw new NoUniqueSessionException(((NoUniqueValueException)cause).getValues());
                    } else {
                        throw new RuntimeException("Unable to derive UID for unexpected cause", cause);
                    }
                } else {
                    report("session", me.getKey(), me.getValue(), sessionLog);
                }
            }

            // prearchivePath is a special case; we set this by hand.
            for (final ExtAttrValue val : setValues(session, sessionValues,
                    sessionBeanBuilders.get(session.getClass()), "prearchivePath")) {
                final String name = val.getName();
                if ("prearchivePath".equals(name)) {
                    setPrearchivePath(session);
                } else {
                    throw new RuntimeException("attribute " + val.getName() + " unexpectedly skipped");
                }
            }

            // Identify the scans -- one scan per value of Series Instance UID
            final SortedMap<String,Map<String,Series>> seriesToUID = Maps.newTreeMap(new Utils.MaybeNumericStringComparator());
            final Set<Map<DicomAttributeIndex,String>> seriesIds;
            final Map<DicomAttributeIndex,ConversionFailureException> failed = Maps.newHashMap();
            seriesIds = store.getUniqueCombinations(Arrays.asList(SeriesNumber, SeriesInstanceUID, SOPClassUID, Modality), failed);
            if (!failed.isEmpty()) {
                sessionLog.log("Unable to retrieve some series-identifying attributes: " + failed);
            }

            for (final Map<DicomAttributeIndex,String> entry : seriesIds) {
                final String seriesInstanceUID = entry.get(SeriesInstanceUID);
                String seriesNumber = entry.get(SeriesNumber);
                if (Strings.isNullOrEmpty(seriesNumber)) {
                    seriesNumber = seriesInstanceUID.replaceAll("[^\\w]", "_");
                }
                final Map<String,Series> uidToSeries;
                if (seriesToUID.containsKey(seriesNumber)) {
                    uidToSeries = seriesToUID.get(seriesNumber);
                } else {
                    uidToSeries = Maps.newTreeMap();
                    seriesToUID.put(seriesNumber, uidToSeries);
                }

                if (!uidToSeries.containsKey(seriesInstanceUID)) {
                    uidToSeries.put(seriesInstanceUID, new Series(seriesNumber, seriesInstanceUID));
                }
                final Series series = uidToSeries.get(seriesInstanceUID);
                series.addModality(entry.get(Modality));
                series.addSOPClass(entry.get(SOPClassUID));
            }

            final Map<String,Series> scanToSeries = Maps.newLinkedHashMap();
            for (final Map.Entry<String,Map<String,Series>> nuse : seriesToUID.entrySet()) {  // Number->UID->Series entry
                final String seriesNumber = nuse.getKey();
                final Map<String,Series> uids = nuse.getValue();
                assert !uids.isEmpty();
                if (uids.size() > 1) {  // each study ID looks like "{SeriesNum}-{Modality}{index}"
                    final Map<String,Integer> modalityCounts = Maps.newHashMap();
                    for (final Series series : uids.values()) {
                        String modality = SOPModel.getLeadModality(series.getModalities());
                        if (null == modality) {
                            modality = UNKNOWN_MODALITY;
                        }
                        final int index;
                        if (modalityCounts.containsKey(modality)) {
                            index = modalityCounts.get(modality) + 1;
                            modalityCounts.put(modality, index);
                        } else {
                            index = 1;
                            modalityCounts.put(modality, index);
                        }
                        final String scanID = new StringBuilder(seriesNumber).append("-").append(modality).append(index).toString();
                        scanToSeries.put(scanID, series);
                    }
                } else {
                    // Just one Series Instance UID for this Series Number; use Series Number as Scan ID
                    scanToSeries.put(seriesNumber, uids.values().iterator().next());
                }
            }

            nScans = scanToSeries.size();

            for (final Map.Entry<String,Series> nse : scanToSeries.entrySet()) {  // Number->Series entry
                try {
                    final Map<DicomAttributeIndex,String> scanSpec = ImmutableMap.of(SeriesInstanceUID, nse.getValue().getUID());
                    final DICOMScanBuilder scanBuilder = DICOMScanBuilder.fromStore(store, sessionLog,
                            nse.getKey(), nse.getValue(), scanSpec, catalogWriterFactory, useRelativePaths());
                    session.addScans_scan(scanBuilder.call());
                } catch (Throwable t) {
                    logger.info("Unable to process scan " + nse.getKey(), t);
                    sessionLog.log("Unable to process scan " + nse.getKey(), t);
                }
            }
            return session;
        } finally {
            if (sessionLog.hasMessages()) {
                logger.info("Warnings occured in processing " + fsdir.getPath() + "; see " + sessionLog);
                sessionLog.close();
            }
        }
    }


    public final String getSessionInfo() {
        final StringBuilder sb = new StringBuilder();
        sb.append(null == nScans ? "(undetermined)" : nScans);
        sb.append(" scans");
        return sb.toString();
    }

    public final void close() throws IOException {
        store.close();
    }

    private static StringBuilder startReport(final Object context, final Object subject) {
        return new StringBuilder(context.toString()).append(" attribute ").append(subject);
    }

    static void report(final Object context, final Object subject, final Exception e, final MicroLog log) {
        try {
            if (e instanceof ConversionFailureException) {
                report(context, subject, (ConversionFailureException)e, log);
            } else if (e instanceof NoUniqueValueException) {
                report(context, subject, (NoUniqueValueException)e, log);
            } else {
                final StringBuilder sb = startReport(context, subject);
                sb.append(" could not be resolved :").append(e);
                log.log(sb.toString());
            }
        } catch (IOException e1) {
            LoggerFactory.getLogger(DICOMSessionBuilder.class).error("Unable to write to session log " + log, e1);
        }
    }

    static void report(final Object context, final Object subject,
            final ConversionFailureException e, final MicroLog log) throws IOException {
        final StringBuilder sb = startReport(context, subject);
        sb.append(": DICOM attribute ").append(e.getAttr());
        sb.append(" has bad value (").append(e.getValue()).append("); unable to derive attribute(s) ");
        sb.append(Arrays.toString(e.getExtAttrs()));
        log.log(sb.toString());
    }

    private static void report(final Object context, final Object subject,
            final NoUniqueValueException e, final MicroLog log) throws IOException {
        final StringBuilder sb = startReport(context, subject);
        if (0 == e.getValues().length) {
            sb.append(" has no value");
        } else {
            sb.append(" has multiple values: ").append(Arrays.toString(e.getValues()));
        }
        log.log(sb.toString());
    }


    static File getCommonRoot(final Iterable<File> files) {
        final Iterator<File> fi = files.iterator();
        File root = fi.next().getParentFile();
        while (fi.hasNext()) {
            root = getCommonRoot(root, fi.next());
        }
        return root;
    }

    private static File getCommonRoot(final File f1, final File f2) {
        if (null == f1) return null;
        for (File f2p = f2.getParentFile(); null != f2p; f2p = f2p.getParentFile()) {
            if (f1.equals(f2p)) return f1;
        }
        return getCommonRoot(f1.getParentFile(), f2);
    }

    private static final String PROJECT_PREFIX = "Project:";
    private static final String SUBJECT_PREFIX = "Subject:";
    private static final String SESSION_PREFIX = "Session:";

    private static final String PROJECT_SPEC_LABEL = "Project";
    private static final String SUBJECT_SPEC_LABEL = "Subject";
    private static final String SESSION_SPEC_LABEL = "Session";
    private static final String ASSIGNMENT = "\\:";
    private static final String LABEL_PATTERN = "\\w+";

    /**
     * Runs the session XML builder on the named files.
     * Uses properties to control behavior:
     * output.dir is the pathname of a directory where XML should be saved
     * @param args roots (directory pathnames) for which XML should be written
     */
    public static void main(String[] args) throws IOException,NoUniqueSessionException,SQLException {
        final XnatAttrDef defaultProjectAttrDef = new ConditionalAttrDef("project",
                new Rule[] {
                new ContainsAssignmentRule(PatientComments,
                        PROJECT_SPEC_LABEL, ASSIGNMENT, LABEL_PATTERN, Pattern.CASE_INSENSITIVE),
                        new ContainsAssignmentRule(StudyComments,
                                PROJECT_SPEC_LABEL, ASSIGNMENT, LABEL_PATTERN, Pattern.CASE_INSENSITIVE),
                                new EqualsRule(StudyDescription),
                                new EqualsRule(AccessionNumber),

        });

        final XnatAttrDef defaultSubjectAttrDef = new ConditionalAttrDef("subject_ID",
                new Rule[] {
                new ContainsAssignmentRule(PatientComments,
                        SUBJECT_SPEC_LABEL, ASSIGNMENT, LABEL_PATTERN, Pattern.CASE_INSENSITIVE),
                        new ContainsAssignmentRule(StudyComments,
                                SUBJECT_SPEC_LABEL, ASSIGNMENT, LABEL_PATTERN, Pattern.CASE_INSENSITIVE),
                                new EqualsRule(PatientName)
        });

        final XnatAttrDef defaultSessionAttrDef = new ConditionalAttrDef("label",
                new Rule[] {
                new ContainsAssignmentRule(PatientComments,
                        SESSION_SPEC_LABEL, ASSIGNMENT, LABEL_PATTERN, Pattern.CASE_INSENSITIVE),
                        new ContainsAssignmentRule(StudyComments,
                                SESSION_SPEC_LABEL, ASSIGNMENT, LABEL_PATTERN, Pattern.CASE_INSENSITIVE),
                                new EqualsRule(PatientID)
        });

        final String outputRootName = System.getProperty("output.dir");
        final File outputRoot = (outputRootName == null) ? null : new File(outputRootName);
        if (outputRoot != null && !outputRoot.isDirectory()) {
            System.err.println("output.dir must be a directory");
            System.exit(-1);
        }

        if (4 < args.length) {
            System.out.println("Usage: SessionBuilder dicom-dir [Project:project] "
                    + "[Subject:subject_id] [Session:session_id]");
            System.exit(-1);
        }

        for (int i = 0; i < args.length; ) {
            final File f = new File(args[i++]);
            final File out = (outputRoot == null) ? new File(f.getPath() + ".xml") : new File(outputRoot, f.getName() + ".xml");
            XnatAttrDef project = defaultProjectAttrDef;
            XnatAttrDef subject = defaultSubjectAttrDef;
            XnatAttrDef session = defaultSessionAttrDef;
            while (i < args.length) {
                String nextArg = args[i++];
                if (nextArg.startsWith(PROJECT_PREFIX)) {
                    project = new XnatAttrDef.Constant("project", nextArg.substring(PROJECT_PREFIX.length()));
                } else if (nextArg.startsWith(SUBJECT_PREFIX)) {
                    subject = new XnatAttrDef.Constant("subject_ID", nextArg.substring(SUBJECT_PREFIX.length()));
                } else if (nextArg.startsWith(SESSION_PREFIX)) {
                    session = new XnatAttrDef.Constant("label", nextArg.substring(SESSION_PREFIX.length()));
                } else {
                    i--;  // not using nextArg here after all
                    break;
                }
            }
            LoggerFactory.getLogger(DICOMSessionBuilder.class).debug("starting");
            final DICOMSessionBuilder psb = new DICOMSessionBuilder(f, new FileWriter(out),
                    new LabelAttrDef(project), new LabelAttrDef(subject), new LabelAttrDef(session));
            psb.setIsInPrearchive(Boolean.parseBoolean(System.getProperty("is.prearc", "true")));
            psb.run();
            psb.close();
        }
    }
}
