/**
 * Copyright (c) 2009-2011 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.Collections;
import java.util.Map;
import java.util.regex.Pattern;

import org.nrg.attr.BasicExtAttrValue;
import org.nrg.attr.ConversionFailureException;
import org.nrg.attr.ExtAttrValue;
import org.nrg.attr.ExtAttrDef.Multiplex;
import org.nrg.dcm.DicomAttributeIndex;
import org.nrg.dcm.xnat.XnatAttrDef.Abstract;

import static org.nrg.dcm.Attributes.*;

/**
 * Multiplex attribute for Echo Time (TE).  If multiple values are present, this is
 * a multiecho scan, and the values go into parameters/addParam fields named
 * MultiEcho_TE{EchoNumber} instead of parameters/te.
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class MREchoTimeAttribute extends Abstract implements XnatAttrDef,
Multiplex<DicomAttributeIndex,String> {
    private static final String meFormat = "MultiEcho_TE%s";

    /**
     * @param name
     * @param attrs
     */
    public MREchoTimeAttribute() {
        super("parameters/te", EchoTime, EchoNumbers);
    }

    /* (non-Javadoc)
     * @see org.nrg.attr.ExtAttrDef.Abstract#convertText(java.util.Map)
     */
    @Override
    public String convertText(final Map<DicomAttributeIndex,String> attrs)
    throws ConversionFailureException {
        if (null == attrs.get(EchoTime)) {
            throw new ConversionFailureException(EchoTime, "null", getName(), "undefined");
        }
        try {
            final float val = Float.parseFloat(attrs.get(EchoTime));
            return Float.toString(val);
        } catch (NumberFormatException e) {
            throw new ConversionFailureException(EchoTime, attrs.get(EchoTime), getName(),
            "not valid real number");
        }
    }


    private static final Pattern nonWordCharsPattern = Pattern.compile("[^\\w]");

    private static String toWordChars(final String in) {
        return null == in ? null : nonWordCharsPattern.matcher(in).replaceAll("_");
    }

    /* (non-Javadoc)
     * @see org.nrg.attr.ExtAttrDef.Multiplex#demultiplex(java.util.Map)
     */
    public ExtAttrValue demultiplex(Map<DicomAttributeIndex,String> vals)
    throws ConversionFailureException {
        return new BasicExtAttrValue("parameters/addParam", convertText(vals),
                Collections.singletonMap("name", String.format(meFormat, toWordChars(vals.get(EchoNumbers)))));
    }

    /* (non-Javadoc)
     * @see org.nrg.attr.ExtAttrDef.Multiplex#getIndexAttribute()
     */
    public DicomAttributeIndex getIndexAttribute() { return EchoNumbers; }
}
