/**
 * Copyright 2011 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.Map;

import org.dcm4che2.data.Tag;
import org.nrg.attr.ConversionFailureException;
import org.nrg.dcm.DicomAttributeIndex;
import org.nrg.dcm.DicomAttributes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
class ExposureTimeAttribute extends XnatAttrDef.Abstract {
    private final Logger logger = LoggerFactory.getLogger(ExposureTimeAttribute.class);

    public ExposureTimeAttribute(final String name, final String modality, int functionalSequenceTag) {
        super(name,
                DicomAttributes.chain(modality + "_ExposureTime", Tag.ExposureTime, functionalSequenceTag),
                DicomAttributes.chain(modality + "_ExposureTime_ms", Tag.ExposureTimeInms, functionalSequenceTag),
                DicomAttributes.chain(modality + "_ExposureTime_us", Tag.ExposureTimeInuS, functionalSequenceTag));
        for (final DicomAttributeIndex dai : getAttrs()) {
            makeOptional(dai);
        }
    }

    /* (non-Javadoc)
     * @see org.nrg.attr.ExtAttrDef#convertText(java.util.Map)
     */
    public String convertText(Map<DicomAttributeIndex,String> vals)
    throws ConversionFailureException {
        ConversionFailureException cfe = null;
        for (final Map.Entry<DicomAttributeIndex,String> me : vals.entrySet()) {
            final String v = me.getValue();
            if (!Strings.isNullOrEmpty(v)) {
                try {
                    final String name = me.getKey().getAttributeName(null);
                    if (name.endsWith("Time") || name.endsWith("Time_ms")) {
                        return Double.toString(Double.parseDouble(v));
                    } else if (name.endsWith("Time_us")) {
                        return Double.toString(Double.parseDouble(v)/1000.0);
                    }
                 } catch (NumberFormatException e) {
                    logger.debug("couldn't parse exposure time from " + v, e);
                    cfe = new ConversionFailureException(me.getKey(), v, "not a valid number");
                }
            }
        }
        if (null != cfe) {
            throw cfe;
        } else {
            throw new ConversionFailureException(this, null, "no exposure time defined");
        }
    }
}
