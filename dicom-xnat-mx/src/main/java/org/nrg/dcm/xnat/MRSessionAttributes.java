/**
 * Copyright (c) 2006,2011 Washington University
 */
package org.nrg.dcm.xnat;


import org.dcm4che2.data.Tag;

import org.nrg.dcm.AttrDefs;
import org.nrg.dcm.MutableAttrDefs;

/**
 * mrSessionData attributes
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
final class MRSessionAttributes {
    private MRSessionAttributes() {}    // no instantiation
    static public AttrDefs get() { return s; }

    static final private MutableAttrDefs s = new MutableAttrDefs(ImageSessionAttributes.get());
    static {
        s.add("UID", Tag.StudyInstanceUID);  
        s.add(new MagneticFieldStrengthAttribute());
        s.add("dcmAccessionNumber", Tag.AccessionNumber);
        s.add("dcmPatientId", Tag.PatientID);
        s.add("dcmPatientName", Tag.PatientName);
    }
}
