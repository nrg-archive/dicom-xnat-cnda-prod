/**
 * Copyright (c) 2010,2011 Washington University
 */
package org.nrg.dcm.xnat;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;
import org.nrg.attr.ExtAttrValue;

import static org.nrg.dcm.Attributes.*;

import com.google.common.collect.ImmutableMap;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class ImageFOVAttributeTest {	
    /**
     * Test method for {@link org.nrg.dcm.xnat.ImageFOVAttribute#ImageFOVAttribute(java.lang.String)}.
     */
    @Test
    public void testImageFOVAttribute() {
        final XnatAttrDef imageFOV = new ImageFOVAttribute("fov");
        assertEquals(Arrays.asList(Rows, Cols), imageFOV.getAttrs());
    }

    /**
     * Test method for {@link org.nrg.dcm.xnat.ImageFOVAttribute#convert(java.util.Map)}.
     */
    @Test
    public void testConvertMapOfDicomAttributeIndexString() throws Exception {
        final XnatAttrDef fovDef = new ImageFOVAttribute("fov");

        final ExtAttrValue fov = fovDef.convert(ImmutableMap.of(Rows, "64", Cols, "80"));
        assertEquals(ImmutableMap.of("x", "80", "y", "64"), fov.getAttrs());
        assertNull(fov.getText());
    }

    /**
     * Test method for {@link org.nrg.dcm.xnat.ImageFOVAttribute#convertText(java.util.Map)}.
     */
    @Test
    public void testConvertTextMapOfDicomAttributeIndexString() throws Exception {
        final XnatAttrDef fovDef = new ImageFOVAttribute("fov");
        final String text = fovDef.convertText(ImmutableMap.of(Rows, "64", Cols, "80"));
        assertNull(text);
    }

}
