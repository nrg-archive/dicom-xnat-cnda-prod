/**
 * Copyright 2010 Washington University
 */
package org.nrg.dcm.xnat;

import static org.junit.Assert.*;

import org.junit.Test;
import org.nrg.xdat.bean.XnatCrscandataBean;
import org.nrg.xdat.bean.XnatCrsessiondataBean;
import org.nrg.xdat.bean.XnatCtscandataBean;
import org.nrg.xdat.bean.XnatCtsessiondataBean;
import org.nrg.xdat.bean.XnatDx3dcraniofacialscandataBean;
import org.nrg.xdat.bean.XnatDx3dcraniofacialsessiondataBean;
import org.nrg.xdat.bean.XnatEcgscandataBean;
import org.nrg.xdat.bean.XnatEcgsessiondataBean;
import org.nrg.xdat.bean.XnatEpsscandataBean;
import org.nrg.xdat.bean.XnatEpssessiondataBean;
import org.nrg.xdat.bean.XnatEsvscandataBean;
import org.nrg.xdat.bean.XnatEsvsessiondataBean;
import org.nrg.xdat.bean.XnatGmvscandataBean;
import org.nrg.xdat.bean.XnatGmvsessiondataBean;
import org.nrg.xdat.bean.XnatHdscandataBean;
import org.nrg.xdat.bean.XnatHdsessiondataBean;
import org.nrg.xdat.bean.XnatIoscandataBean;
import org.nrg.xdat.bean.XnatIosessiondataBean;
import org.nrg.xdat.bean.XnatMgscandataBean;
import org.nrg.xdat.bean.XnatMgsessiondataBean;
import org.nrg.xdat.bean.XnatMrscandataBean;
import org.nrg.xdat.bean.XnatMrsessiondataBean;
import org.nrg.xdat.bean.XnatNmscandataBean;
import org.nrg.xdat.bean.XnatNmsessiondataBean;
import org.nrg.xdat.bean.XnatOpscandataBean;
import org.nrg.xdat.bean.XnatOpsessiondataBean;
import org.nrg.xdat.bean.XnatOptscandataBean;
import org.nrg.xdat.bean.XnatOptsessiondataBean;
import org.nrg.xdat.bean.XnatOtherdicomscandataBean;
import org.nrg.xdat.bean.XnatOtherdicomsessiondataBean;
import org.nrg.xdat.bean.XnatPetscandataBean;
import org.nrg.xdat.bean.XnatPetsessiondataBean;
import org.nrg.xdat.bean.XnatRfscandataBean;
import org.nrg.xdat.bean.XnatRfsessiondataBean;
import org.nrg.xdat.bean.XnatRtimagescandataBean;
import org.nrg.xdat.bean.XnatRtsessiondataBean;
import org.nrg.xdat.bean.XnatScscandataBean;
import org.nrg.xdat.bean.XnatUsscandataBean;
import org.nrg.xdat.bean.XnatUssessiondataBean;
import org.nrg.xdat.bean.XnatXascandataBean;
import org.nrg.xdat.bean.XnatXasessiondataBean;
import org.nrg.xdat.bean.XnatXcvscandataBean;
import org.nrg.xdat.bean.XnatXcvsessiondataBean;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class XnatClassMappingTest {

	/**
	 * Test method for {@link org.nrg.dcm.xnat.XnatClassMapping#getBeanClass(java.lang.String)}.
	 */
	@Test
	public void testgetBeanClass() throws ClassNotFoundException {
		assertEquals(XnatMrscandataBean.class, XnatClassMapping.getBeanClass("mrScanData"));
		assertEquals(XnatCrscandataBean.class, XnatClassMapping.getBeanClass("crScanData"));
		assertEquals(XnatCtscandataBean.class, XnatClassMapping.getBeanClass("ctScanData"));
		assertEquals(XnatDx3dcraniofacialscandataBean.class, XnatClassMapping.getBeanClass("dx3DCraniofacialScanData"));
		assertEquals(XnatEcgscandataBean.class, XnatClassMapping.getBeanClass("ecgScanData"));
		assertEquals(XnatEpsscandataBean.class, XnatClassMapping.getBeanClass("epsScanData"));
		assertEquals(XnatEsvscandataBean.class, XnatClassMapping.getBeanClass("esvScanData"));
		assertEquals(XnatGmvscandataBean.class, XnatClassMapping.getBeanClass("gmvScanData"));
		assertEquals(XnatHdscandataBean.class, XnatClassMapping.getBeanClass("hdScanData"));
		assertEquals(XnatIoscandataBean.class, XnatClassMapping.getBeanClass("ioScanData"));
		assertEquals(XnatMgscandataBean.class, XnatClassMapping.getBeanClass("mgScanData"));
		assertEquals(XnatNmscandataBean.class, XnatClassMapping.getBeanClass("nmScanData"));
		assertEquals(XnatOpscandataBean.class, XnatClassMapping.getBeanClass("opScanData"));
		assertEquals(XnatMgscandataBean.class, XnatClassMapping.getBeanClass("mgScanData"));
		assertEquals(XnatOptscandataBean.class, XnatClassMapping.getBeanClass("optScanData"));
		assertEquals(XnatOtherdicomscandataBean.class, XnatClassMapping.getBeanClass("otherDicomScanData"));
		assertEquals(XnatPetscandataBean.class, XnatClassMapping.getBeanClass("petScanData"));
		assertEquals(XnatRfscandataBean.class, XnatClassMapping.getBeanClass("rfScanData"));
		assertEquals(XnatRtimagescandataBean.class, XnatClassMapping.getBeanClass("rtImageScanData"));
		assertEquals(XnatScscandataBean.class, XnatClassMapping.getBeanClass("scScanData"));
		assertEquals(XnatUsscandataBean.class, XnatClassMapping.getBeanClass("usScanData"));
		assertEquals(XnatXascandataBean.class, XnatClassMapping.getBeanClass("xaScanData"));
		assertEquals(XnatXcvscandataBean.class, XnatClassMapping.getBeanClass("xcvScanData"));
		
		assertEquals(XnatMrsessiondataBean.class, XnatClassMapping.getBeanClass("mrSessionData"));
		assertEquals(XnatCrsessiondataBean.class, XnatClassMapping.getBeanClass("crSessionData"));
		assertEquals(XnatCtsessiondataBean.class, XnatClassMapping.getBeanClass("ctSessionData"));
		assertEquals(XnatDx3dcraniofacialsessiondataBean.class, XnatClassMapping.getBeanClass("dx3DCraniofacialSessionData"));
		assertEquals(XnatEcgsessiondataBean.class, XnatClassMapping.getBeanClass("ecgSessionData"));
		assertEquals(XnatEpssessiondataBean.class, XnatClassMapping.getBeanClass("epsSessionData"));
		assertEquals(XnatEsvsessiondataBean.class, XnatClassMapping.getBeanClass("esvSessionData"));
		assertEquals(XnatGmvsessiondataBean.class, XnatClassMapping.getBeanClass("gmvSessionData"));
		assertEquals(XnatHdsessiondataBean.class, XnatClassMapping.getBeanClass("hdSessionData"));
		assertEquals(XnatIosessiondataBean.class, XnatClassMapping.getBeanClass("ioSessionData"));
		assertEquals(XnatMgsessiondataBean.class, XnatClassMapping.getBeanClass("mgSessionData"));
		assertEquals(XnatNmsessiondataBean.class, XnatClassMapping.getBeanClass("nmSessionData"));
		assertEquals(XnatOpsessiondataBean.class, XnatClassMapping.getBeanClass("opSessionData"));
		assertEquals(XnatMgsessiondataBean.class, XnatClassMapping.getBeanClass("mgSessionData"));
		assertEquals(XnatOptsessiondataBean.class, XnatClassMapping.getBeanClass("optSessionData"));
		assertEquals(XnatOtherdicomsessiondataBean.class, XnatClassMapping.getBeanClass("otherDicomSessionData"));
		assertEquals(XnatPetsessiondataBean.class, XnatClassMapping.getBeanClass("petSessionData"));
		assertEquals(XnatRfsessiondataBean.class, XnatClassMapping.getBeanClass("rfSessionData"));
		assertEquals(XnatRtsessiondataBean.class, XnatClassMapping.getBeanClass("rtSessionData"));
		assertEquals(XnatUssessiondataBean.class, XnatClassMapping.getBeanClass("usSessionData"));
		assertEquals(XnatXasessiondataBean.class, XnatClassMapping.getBeanClass("xaSessionData"));
		assertEquals(XnatXcvsessiondataBean.class, XnatClassMapping.getBeanClass("xcvSessionData"));
	}
	
	@Test(expected=ClassNotFoundException.class)
	public void testNoSuchBeanClass() throws ClassNotFoundException {
		XnatClassMapping.getBeanClass("scSessionData");
	}
}
