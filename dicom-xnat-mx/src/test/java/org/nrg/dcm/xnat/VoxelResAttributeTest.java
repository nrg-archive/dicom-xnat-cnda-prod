package org.nrg.dcm.xnat;

import static org.junit.Assert.*;

import org.junit.Test;
import org.nrg.attr.ExtAttrValue;

import static org.nrg.dcm.DicomAttributes.*;

import com.google.common.collect.ImmutableMap;

public class VoxelResAttributeTest {
    @Test
    public void testConvertMapOfDicomAttributeIndexString() throws Exception {
        final XnatAttrDef voxelResDef = new VoxelResAttribute("voxelRes");
        final ExtAttrValue voxelRes = voxelResDef.convert(ImmutableMap.of(PIXEL_SPACING, "1\\1.5", SLICE_THICKNESS, "2"));
        assertNull(voxelRes.getText());
        assertEquals(ImmutableMap.of("x", "1.0", "y", "1.5", "z", "2.0"), voxelRes.getAttrs());
    }

    @Test
    public void testConvertTextMapOfDicomAttributeIndexString() throws Exception {
        final XnatAttrDef voxelResDef = new VoxelResAttribute("voxelRes");
        final String text = voxelResDef.convertText(ImmutableMap.of(PIXEL_SPACING, "1\\1.5", SLICE_THICKNESS, "2"));
        assertNull(text);
    }
}
