/**
 * Copyright (c) 2011 Washington University
 */
package org.nrg.dcm.xnat;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.HashMap;

import org.junit.Test;
import org.nrg.attr.ConversionFailureException;
import org.nrg.attr.ExtAttrValue;
import org.nrg.dcm.DicomAttributeIndex;

import static org.nrg.dcm.xnat.MagneticFieldStrengthAttribute.*;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class MagneticFieldStrengthAttributeTest {
    private final XnatAttrDef fs = new MagneticFieldStrengthAttribute();
    
    @Test
    public void testName() throws ConversionFailureException {
        final ExtAttrValue v = fs.convert(Collections.singletonMap(MAGNETIC_FIELD_STRENGTH, "3.0"));
        assertEquals("fieldStrength", v.getName());
    }
    
    @Test
    public void testConvertTrivial() throws ConversionFailureException {
        final ExtAttrValue v = fs.convert(Collections.singletonMap(MAGNETIC_FIELD_STRENGTH, "1.5"));
        assertTrue(v.getAttrs().isEmpty());
        assertEquals("1.5", v.getText());
    }

    @Test
    public void testConvertRounding() throws ConversionFailureException {
        final ExtAttrValue v1 = fs.convert(Collections.singletonMap(MAGNETIC_FIELD_STRENGTH, "1.49997"));
        assertTrue(v1.getAttrs().isEmpty());
        assertEquals("1.5", v1.getText());
        
        final ExtAttrValue v2 = fs.convert(Collections.singletonMap(MAGNETIC_FIELD_STRENGTH, "2.893"));
        assertTrue(v2.getAttrs().isEmpty());
        assertEquals("3.0", v2.getText());
    }

    @Test
    public void testConvertFromGauss() throws ConversionFailureException {
        final ExtAttrValue v = fs.convert(Collections.singletonMap(MAGNETIC_FIELD_STRENGTH, "14996"));
        assertTrue(v.getAttrs().isEmpty());
        assertEquals("1.5", v.getText());
    }
    
    @Test(expected = ConversionFailureException.class)
    public void testInvalidValue() throws ConversionFailureException {
        fs.convert(Collections.singletonMap(MAGNETIC_FIELD_STRENGTH, "foo"));
    }
    
    @Test(expected = ConversionFailureException.class)
    public void testNullValue() throws ConversionFailureException {
        fs.convert(new HashMap<DicomAttributeIndex,String>());
    }
}
