/**
 * Copyright (c) 2010,2011 Washington University
 */
package org.nrg.dcm.xnat;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;

import static org.nrg.dcm.DicomAttributes.*;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class OrientationAttributeTest {
    /**
     * Test method for {@link org.nrg.dcm.xnat.OrientationAttribute#convertText(java.util.Map)}.
     */
    @Test
    public void testConvertTextMapOfDicomAttributeIndexString() throws Exception {
        final XnatAttrDef orientDef = new OrientationAttribute("orientation");
        assertEquals("Sag", orientDef.convertText(Collections.singletonMap(IMAGE_ORIENTATION_PATIENT, "0\\1\\0\\0\\0\\1")));
        assertEquals("Cor", orientDef.convertText(Collections.singletonMap(IMAGE_ORIENTATION_PATIENT, "0\\0\\1\\1\\0\\0")));
        assertEquals("Tra", orientDef.convertText(Collections.singletonMap(IMAGE_ORIENTATION_PATIENT, "1\\0\\0\\0\\1\\0")));
    }

}
