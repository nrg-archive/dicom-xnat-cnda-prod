/**
 * Copyright (c) 2009-2011 Washington University
 */
package org.nrg.dcm.xnat;

import java.util.Collections;

import static org.nrg.dcm.Attributes.*;

import junit.framework.TestCase;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class LabelAttrDefTest extends TestCase {
    /**
     * Test method for {@link org.nrg.dcm.xnat.LabelAttrDef#convertText(java.util.Map)}.
     */
    public void testConvertTextMapOfIntegerString() throws Exception {
        final XnatAttrDef def = new LabelAttrDef(new XnatAttrDef.Text("test", StudyDescription));

        assertEquals("value_with_underscores",
                def.convertText(Collections.singletonMap(StudyDescription, "value_with_underscores")));
        assertEquals("value-with-hyphens",
                def.convertText(Collections.singletonMap(StudyDescription, "value-with-hyphens")));
        assertEquals("mixed-value_01",
                def.convertText(Collections.singletonMap(StudyDescription, "mixed-value_01")));
        assertEquals("____remap____invalid____chars____",
                def.convertText(Collections.singletonMap(StudyDescription, "!@#$remap%^&*invalid()+=chars{}[]")));
    }
}
