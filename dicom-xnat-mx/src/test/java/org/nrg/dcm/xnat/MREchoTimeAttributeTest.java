/**
 * Copyright (c) 2010,2011 Washington University
 */
package org.nrg.dcm.xnat;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;
import org.nrg.attr.ExtAttrValue;
import org.nrg.attr.ExtAttrDef.Multiplex;
import org.nrg.dcm.DicomAttributeIndex;

import static org.nrg.dcm.Attributes.*;

import com.google.common.collect.ImmutableMap;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class MREchoTimeAttributeTest {
    /**
     * Test method for {@link org.nrg.dcm.xnat.MREchoTimeAttribute#MREchoTimeAttribute()}.
     */
    @Test
    public void testMREchoTimeAttribute() {
        final XnatAttrDef tedef = new MREchoTimeAttribute();
        assertEquals(Arrays.asList(EchoTime, EchoNumbers), tedef.getAttrs());
    }

    /**
     * Test method for {@link org.nrg.dcm.xnat.MREchoTimeAttribute#convertText(java.util.Map)}.
     */
    @Test
    public void testConvertTextMapOfDicomAttributeIndexString() throws Exception {
        final XnatAttrDef tedef = new MREchoTimeAttribute();
        final String text = tedef.convertText(ImmutableMap.of(EchoTime, "1500.0", EchoNumbers, "1"));
        assertEquals(text, "1500.0");
    }

    /**
     * Test method for {@link org.nrg.dcm.xnat.MREchoTimeAttribute#demultiplex(java.util.Map)}.
     */
    @Test
    public void testDemultiplex() throws Exception {
        final Multiplex<DicomAttributeIndex,String> tedef = new MREchoTimeAttribute();
        final ExtAttrValue dmv = tedef.demultiplex(ImmutableMap.of(EchoTime, "1200.0", EchoNumbers, "1"));
        assertEquals("parameters/addParam", dmv.getName());
        assertEquals("1200.0", dmv.getText());
        assertEquals(ImmutableMap.of("name", "MultiEcho_TE1"), dmv.getAttrs());
    }

    /**
     * Test method for {@link org.nrg.dcm.xnat.MREchoTimeAttribute#getIndexAttribute()}.
     */
    @Test
    public void testGetIndexAttribute() {
        final Multiplex<DicomAttributeIndex,String> tedef = new MREchoTimeAttribute();
        assertEquals(EchoNumbers, tedef.getIndexAttribute());
    }
}
