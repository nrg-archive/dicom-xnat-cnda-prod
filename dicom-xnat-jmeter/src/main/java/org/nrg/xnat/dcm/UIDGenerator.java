/**
 * Copyright (c) 2010,2011 Washington University
 */
package org.nrg.xnat.dcm;

import java.math.BigInteger;
import java.util.UUID;
import java.util.concurrent.Callable;

/**
 * Generates fresh UIDs by converting a randomly generated UUID to an OID as per ITU-T X.667
 * @author Kevin A. Archie <karchie@wustl.edu>
 */
public final class UIDGenerator implements Callable<String> {
    public final static String name = "UID";
    public final static String root = "2.25.";

    public BigInteger unsigned(final long l) {
        if (l > 0) {
            return BigInteger.valueOf(l);
        } else {
            return BigInteger.valueOf(l & Long.MAX_VALUE).setBit(63);
        }
    }
    
    /*
     * (non-Javadoc)
     * @see java.util.concurrent.Callable#call()
     */
    public String call() {
        final UUID uuid = UUID.randomUUID();
        final StringBuilder sb = new StringBuilder(root);
        sb.append(unsigned(uuid.getMostSignificantBits()).shiftLeft(64).add(unsigned(uuid.getLeastSignificantBits())));
        return sb.toString();
    }
}
