/**
 * Copyright (c) 2011 Washington University
 */
package org.nrg.net;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class SimpleAuthenticator extends Authenticator {
    private final PasswordAuthentication auth;
    
    public SimpleAuthenticator(final PasswordAuthentication auth) {
        this.auth = auth;
    }
    
    public SimpleAuthenticator(final String username, final String password) {
        this(new PasswordAuthentication(username, password.toCharArray()));
    }

    /*
     * (non-Javadoc)
     * @see java.net.Authenticator#getPasswordAuthentication()
     */
    public PasswordAuthentication getPasswordAuthentication() {
        return auth;
    }
}
