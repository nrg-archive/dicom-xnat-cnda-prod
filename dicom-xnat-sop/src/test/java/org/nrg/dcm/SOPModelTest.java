/**
 * Copyright (c) 2010 Washington University
 */
package org.nrg.dcm;

import static org.junit.Assert.*;

import org.dcm4che2.data.UID;
import org.junit.Test;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public class SOPModelTest {

	/**
	 * Test method for {@link org.nrg.dcm.SOPModel#getScanType(java.lang.String)}.
	 */
	@Test
	public void testGetScanType() {
		assertEquals("mrScanData", SOPModel.getScanType(UID.MRImageStorage));
		assertEquals("mrScanData", SOPModel.getScanType(UID.EnhancedMRImageStorage));
		
		assertEquals("petScanData", SOPModel.getScanType(UID.PositronEmissionTomographyImageStorage));
		assertEquals("petScanData", SOPModel.getScanType(UID.EnhancedPETImageStorage));
		
		assertEquals("ctScanData", SOPModel.getScanType(UID.CTImageStorage));
		assertEquals("ctScanData", SOPModel.getScanType(UID.EnhancedCTImageStorage));
		
		assertEquals("xaScanData", SOPModel.getScanType(UID.XRayAngiographicImageStorage));
		
		assertEquals("usScanData", SOPModel.getScanType(UID.UltrasoundImageStorage));
		assertEquals("usScanData", SOPModel.getScanType(UID.UltrasoundMultiframeImageStorage));
		
		assertEquals("rtImageScanData", SOPModel.getScanType(UID.RTImageStorage));
		assertEquals("rtImageScanData", SOPModel.getScanType(UID.RTDoseStorage));
		
		assertEquals("crScanData", SOPModel.getScanType(UID.ComputedRadiographyImageStorage));
		
		assertEquals("optScanData", SOPModel.getScanType(UID.OphthalmicTomographyImageStorage));
		
		assertEquals("scScanData", SOPModel.getScanType(UID.SecondaryCaptureImageStorage));
		
		assertEquals("otherDicomScanData", SOPModel.getScanType(UID.BasicTextSRStorage));
		assertEquals("otherDicomScanData", SOPModel.getScanType(UID.EnhancedSRStorage));
		assertEquals("otherDicomScanData", SOPModel.getScanType(UID.SiemensCSANonImageStorage));
		
		assertEquals(null, SOPModel.getScanType("NoSuchSOPClass"));
		
		assertEquals("ctScanData", SOPModel.getScanType(UID.CTImageStorage, UID.SecondaryCaptureImageStorage));
		assertEquals("petScanData", SOPModel.getScanType(UID.PositronEmissionTomographyImageStorage, UID.CTImageStorage));
	}
	

	@Test
	public void testIsPrimaryImagingType() {
		assertTrue(SOPModel.isPrimaryImagingSOP(UID.MRImageStorage));
		assertTrue(SOPModel.isPrimaryImagingSOP(UID.CTImageStorage));
		assertFalse(SOPModel.isPrimaryImagingSOP(UID.SecondaryCaptureImageStorage));
	}
}
