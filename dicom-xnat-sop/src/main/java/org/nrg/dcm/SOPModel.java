/**
 * Copyright (c) 2010,2011 Washington University
 */
package org.nrg.dcm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.dcm4che2.data.UID;

import com.google.common.collect.ImmutableMap;

/**
 * @author Kevin A. Archie <karchie@wustl.edu>
 *
 */
public final class SOPModel {
    public static final String XNAT_SCAN_COLUMN = "XNAT_SCAN_ID";

    private SOPModel() {}

    private static final String SOP_TO_SCAN_TYPE_R = "/series-scans.properties";
    private static final String PRIMARY_SOP_UID_R = "/primary-sops.txt";

    private static final ImmutableMap<String,String> SOP_TO_SCAN_TYPE = buildSOPtoScanType();
    private static final Set<String> PRIMARY_SOP_UIDS = getPrimarySOPUIDs();

    private static final String[] LEAD_SOP_CLASSES = {
        UID.EnhancedPETImageStorage,
        UID.PositronEmissionTomographyImageStorage,
        UID.EnhancedMRImageStorage,
        UID.MRImageStorage,
        UID.EnhancedCTImageStorage,
        UID.CTImageStorage,
        UID.XRayAngiographicImageStorage,
        UID.UltrasoundImageStorage,
        UID.UltrasoundMultiframeImageStorage,
        UID.RTImageStorage,
        UID.RTDoseStorage,
        UID.ComputedRadiographyImageStorage,
        UID.OphthalmicTomographyImageStorage
    };


    private static final String[] LEAD_SCAN_TYPES = {
        "petScanData",
        "mrScanData",
        "ctScanData",
        "xaScanData",
        "usScanData",
        "rtImageScanData",
        "crScanData",
        "optScanData",
        "scScanData",
        "otherDicomScanData"
    };

    private static final String[] LEAD_MODALITIES = {
        "PT", "MR", "CT", "XA", "US", "RT", "CR", "OPT", "SC", "OT"
    };

    private static String getUIDForName(final String name) {
        try {
            final Field f = UID.class.getDeclaredField(name);
            return (String)f.get(null);
        } catch (SecurityException e) {
            throw new RuntimeException(e);
        } catch (IllegalArgumentException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (NoSuchFieldException e) {
            return name;
        }
    }

    private static ImmutableMap<String,String> buildSOPtoScanType() {
        try {
            final Properties p = new Properties();
            final InputStream in = SOPModel.class.getResourceAsStream(SOP_TO_SCAN_TYPE_R);
            if (null == in) {
                throw new RuntimeException("resource " + SOP_TO_SCAN_TYPE_R + " not found");
            }
            IOException ioexception = null;
            try {
                p.load(in);
            } catch (IOException e) {
                throw ioexception = e;
            } finally {
                try {
                    in.close();
                } catch (IOException e) {
                    throw ioexception = null == ioexception ? e : ioexception;
                }
            }

            final ImmutableMap.Builder<String,String> builder = ImmutableMap.builder();
            for (final Map.Entry<Object,Object> me : p.entrySet()) {
                final String uidName = me.getKey().toString();
                final String uid = getUIDForName(uidName);
                builder.put(uid, me.getValue().toString());
            }
            return builder.build();
        } catch (IOException e) {
            throw new RuntimeException("Unable to initialize SOP to XNAT model conversion", e);
        }
    }

    private static final Set<String> getPrimarySOPUIDs() {
        try {
            final InputStream in = SOPModel.class.getResourceAsStream(PRIMARY_SOP_UID_R);
            if (null == in) {
                throw new RuntimeException("resource " + PRIMARY_SOP_UID_R + " not found");
            }
            IOException ioexception = null;
            try {
                final BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                try {
                    final Set<String> types = new HashSet<String>();
                    for (String line = reader.readLine(); null != line; line = reader.readLine()) {
                        types.add(getUIDForName(line.trim()));
                    }
                    return Collections.unmodifiableSet(types);
                } catch (IOException e) {
                    throw ioexception = e;
                } finally {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        throw ioexception = null == ioexception ? e : ioexception;
                    }
                }
            } finally {
                try {
                    in.close();
                } catch (IOException e) {
                    throw null == ioexception ? e : ioexception;
                }
            }
        } catch (IOException e) {
            throw new RuntimeException("Unable to read primary imaging types", e);
        }
    }

    private static <T> T getLead(final T[] options, final Set<? extends T> values) {
        for (final T t : options) {
            if (values.contains(t)) {
                return t;
            }
        }
        return null;
    }


    /**
     * Returns the XSD type of the XNAT scan model corresponding
     * to the given SOP Class UID
     * @param sopClassUID
     * @return XNAT scan model type (with no namespace prefix)
     */
    public static String getScanType(final String sopClassUID) {
        return SOP_TO_SCAN_TYPE.get(sopClassUID);
    }

    /**
     * Returns the XSD type of the XNAT scan model for a series
     * containing the given SOP Class UIDs.
     * @param sopClassUIDs
     * @return XNAT scan model type (with no namespace prefix)
     */
    public static String getScanType(final String...sopClassUIDs) {
        return getScanType(Arrays.asList(sopClassUIDs));
    }

    /**
     * Returns the XSD type of the XNAT scan model for a series
     * containing the given SOP Class UIDs.
     * @param sopClassUIDs
     * @return XNAT scan model type (with no namespace prefix)
     */
    public static String getScanType(final Iterable<String> sopClassUIDs) {
        final Set<String> scanTypes = new LinkedHashSet<String>();
        for (final String uid : sopClassUIDs) {
            scanTypes.add(SOP_TO_SCAN_TYPE.get(uid));
        }
        return getLead(LEAD_SCAN_TYPES, scanTypes);		
    }

    /**
     * Is the given SOP class a primary imaging type?
     * @param sopClassUID
     * @return true if the named SOP class is a primary imaging type
     */
    public static boolean isPrimaryImagingSOP(final String sopClassUID) {
        return PRIMARY_SOP_UIDS.contains(sopClassUID);
    }

    /**
     * Returns the DICOM modality identifer (as defined in PS 3.3, C.7.3.1.1.1)
     * that is dominant in XNAT among the provided modalities.
     * @param modalities Set of DICOM modality identifiers
     * @return DICOM modality that dominates in an XNAT representation
     */
    public static String getLeadModality(final Set<String> modalities) {
        return getLead(LEAD_MODALITIES, modalities);
    }


    /**
     * Returns the DICOM SOP Class UID dominant among the given class UIDs.
     * @param classes Set of DICOM SOP Class UIDs
     * @return DICOM SOP Class UID that dominates in an XNAT representation
     */
    public static String getLeadSOPClass(final Set<String> classes) {
        return getLead(LEAD_SOP_CLASSES, classes);
    }
}
